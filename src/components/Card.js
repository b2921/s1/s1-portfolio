import React from 'react';

import {Card, Button} from 'react-bootstrap'

export default function CardComponent () {
	return(

		<Card>
			<Card.Body>
				<Card.Title>This is a Card Title</Card.Title>
				<Card.Text>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud
					</p>
				</Card.Text>
				<Button variant = "success">Go Somewhere</Button>
			</Card.Body>
		</Card>


		)
}